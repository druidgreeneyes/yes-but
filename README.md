# Yes, but...

This is an improvisational, gm-less quasi-rpg in which the players start with no knowledge of their world, their objectives, or even themselves, and build everything up from scratch during play.

TODO:
- [ ] Better rules description
- [ ] Better names for participants (to replace answerer, asker-slash-actor, etc)

## How to Play
### You will need:
- Something with which to adjudicate a randomized binary
    - flip a coin, roll a die, whatever you like)
- Something with which to construct conceptual maps of what you have built so far
    - index cards work great
- Something with which to identify the current asker-slash-actor, and something else with which to identify the current answerer.

### You will do:
Decide who will ask the first question and who will answer it, and begin play.

Play takes the following steps:
1. Asker either 
    - asks a yes or no question
        - Are we on a deserted island?
        - Are we members of a long-lost race of space-faring moles?
        - Does any of us have a dollar?
    - Or takes an action
        - I spend a long time peering through my timescope in search of our lost brethren!
        - I rub my energy legs together fast enough to start a fire!
        - I use my dollar to buy a soda!
2. If...
    - the asker took an action: there is no 2! Pass the asker-slash-actor token to the next player.
    - the asker asked a question: the answerer generates a random binary value with their random binary value generator.
        - On a truthy value, the answerer responds "Yes, but..." and then makes up a downside, or a contrasting event, or something similar:
            - Are we on a deserted island? Yes...but we don't think we're alone...
            - Does any of us have a dollar? Yes...but it's stuck in a clear box beneath six layers of padlocks and a genuinely intimidating red button.
        - On a falsey value, the answerer response "No, but..." and then makes up an upside, contrasting event, or something similar:
            - Are we on a deserted island? No...but our greatest enemy is, and we'll have to go there if we want to defeat them.
            - Are we members of a long-lost race of space-faring moles? No...but we are members of a long-lost race of sea-faring moles!
        - In either case, pass the asker-slash-actor token to the next player, and pass the answerer token to the next answerer.
            - The answerer token should only move if the person holding it has just answered a question EXCEPT in the event that somebody tries to pass the asker-slash-actor token to the answerer, in which case the two players trade tokens (asker-slash-actor becomes answerer, and vice versa)

Here end the rules. That's it. Keep playing unti you get tired of it or the group settles on a satisfying conclusion. Don't forget to map things with your index cards, as it will be difficult to remember all the yes-buts and no-buts.

## Other stuff
I would like this to have just a *skeetch* more structure; the idea was that the game would begin with world building, character buiding, objective-setting, etc, and transition over the course of the session away from asking questions all the time and toward taking actions in order to accomplish one or more overarching goals. The first play through was a blast, but it ended up being kind of a mess of plot twists and mind-control, and we got to the end by repeatedy taking actions and then asking 'does that one work?' until we finally got a yes. The yes-but/no-but format does not lend itself easily to endings, for fairly obvious reasons. So, if you have tried it once and want to make things a little different or a little more self-sufficient, here are some things you could try (as of this writing, I have not tried any of them, so ymmv):

### Establishing Shots
Start with one index card in front of each player and two index cards in the middle of the table, one labelled 'Goal' and the other labelled 'Start', with no other content. Players invent names to write on their individual cards. To begin with, only allow questions that contribute to A) setting the start or the goal, B) building out the world, or C) establishing characters. This phase lasts until the following criteria are satisfied:
- The 'Goal' card has something acceptable on it
- The 'Start' card has something acceptable on it
- Each player card has something on it that is present on no other player card.

At which point all questions are fair game and players may take any actions they wish that don't seem egregious or unreasonable to the rest of the group.

This format can be made more or less complicated as you desire by simply adding or removing cards that have to be filled out. Maybe you just want to do the Start and Goal cards, or maybe you want a Start and a Goal, all the players, and at least six History cards that describe how the party has got to where it is now.

### The metagame
Write the rules of this game on some index cards and place them on the table. During play, players may cause the rules to change by asking about them:
- Do we have a 50% chance of getting 'and' instead of 'but'? No, but you do have a 10% chance, and every time someone gets an 'and' they also recieve a bonus point to spend on whatever they wish during the game.

Whenever the rules change, add new index cards or remove existing ones to make sure that what is visible on the board is accurate. This format will probably begin to remind people of the game, 1000 Blank White Cards.